SUBROUTINE sub_lm_initialize
! Monte carlo model for particle deposition in lung
! 
 USE lungmodel_properties
 
 IMPLICIT NONE

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
! Parameters for random numbers
 REAL ( kind = 4 ) fn(128)
 INTEGER ( kind = 4 ) kn(128)
 REAL ( kind = 4 ) r4_nor
 INTEGER ( kind = 4 ) seed
 REAL ( kind = 4 ) value
 REAL ( kind = 4 ) wn(128)
 DOUBLE PRECISION :: rand
 DOUBLE PRECISION :: a1, a2, a3
 DOUBLE PRECISION :: start_time, end_time
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 CALL CPU_TIME(start_time)
 
 WRITE (*,*) 'Hello World'

 WRITE (*,*) 'Intialising data'    
 
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 nal(:) = 0.D0
 genalv(:) = 0.D0
 totalv(:) = 0.D0
 totalvfrac(:) = 0.D0
 totala(:) = 0.D0
 dtotalvdz(:) = 0.D0
 alpha(:) = 0.D0
 gendep(:) = 0.D0

 gendia(:) = 0.D0
 genlen(:) = 0.D0
 genarea(:) = 0.D0
 sumarea(:) = 0.D0
 avpul(:) = 0.D0

 genQa(:) = 0.D0 
 genu(:) = 0.D0
 genstk(:) = 0.D0
 
 nide(:) = 0.D0
 ndde(:) = 0.D0
 nsde(:) = 0.D0
 nde(:) = 0.D0
 Pdep(:) = 0.D0

 checknrandoms = 0.D0
! asincount(:) = 0.D0

 gen17count(:) = 0.D0
 gen18count(:) = 0.D0
 gen19count(:) = 0.D0
 gen20count(:) = 0.D0
 gen21count(:) = 0.D0
 gen22count(:) = 0.D0
 gen23count(:) = 0.D0

 airwaymax(:) = 0.D0

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! PART 0 - Initialize data - Compute alveoli parameters
 WRITE (*,*) 'Compute alveolar parameters'

! No of alveolar branches in each generation, from 17 to 23
 nal(17) = 5
 nal(18) = 8
 nal(19) = 12
 nal(20) = 20
 nal(21) = 20
 nal(22) = 20
 nal(23) = 17
 
 do01: DO i = 17, 23
 
   genalv(i) = nal(i) * (2**i) * Vaa
   
   totalv(i) = totalv(i-1) + genalv(i)

 END DO do01
 
! compute gradient
! for the first term
 i = 0
 dtotalvdz(i) = (totalv(i+1) - totalv(i))/totalv(numgen)
 
 totalvfrac(i) = totalv(i)/totalv(numgen)

! for other terms
 do02: DO i = 1, numgen
 
 dtotalvdz(i) = (totalv(i) - totalv(i-1))/totalv(numgen)
 
 totalvfrac(i) = totalv(i)/totalv(numgen)
 
 END DO do02

 WRITE (*,*) 'Write alveolar parameters to file'
 
! Write this data to file 
! compute diameters
! OPEN (UNIT = 555, FILE = '666-nalevoli.dat', STATUS = 'REPLACE', FORM = 'FORMATTED')

! WRITE (555,*) "#generation, number of alveoli, generation alveoli volume, total alveoli volume till than generation, fraction of total volume till that generation, gradient at that generation"
 
! do03: DO i = 0, numgen

! WRITE (555,*) i, nal(i), genalv(i), totalv(i), totalvfrac(i) , dtotalvdz(i)
 
! END DO do03

! CLOSE(555) 
 
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! PART 1 - Finding generation diameter, length and saving to data
 WRITE (*,*) 'Compute diameter, length of generations'
 
! First four generations have one formula
 do11: DO i=0,3
 
!   WRITE (*,*) 'i value is', i
   gendia(i) = d00 * dexp(-p3 * i)
   
   genlen(i) = l0 * dexp(p1 * i)
  
 END DO do11
 
! Next generations have a different formula
 do12: DO i=4,  numgen
 
!   WRITE (*,*) 'i value is', i
   gendia(i) = d01 * dexp(-(p4 - p5*i)*i)
   
   genlen(i) = l0p * dexp(p2 * i)
 
 END DO do12

 WRITE (*,*) 'Write diameter, length of generations to file'

! Writing this data to file
! OPEN (UNIT = 555, FILE = '666-gendia.dat', STATUS = 'REPLACE', FORM = 'FORMATTED')
 
! WRITE (555,*) "#generation, generation diameter"

! do13: DO i = 0,  numgen

!   WRITE (555,*) i, gendia(i)

! END DO do13
 
! Writing this data to file
! OPEN (UNIT = 555, FILE = '666-genlen.dat', STATUS = 'REPLACE', FORM = 'FORMATTED')
 
! WRITE (555,*) "#generation, generation length"

! do13b: DO i = 0,  numgen

!   WRITE (555,*) i, genlen(i)

! END DO do13b

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! PART 2 - Finding area of each generation

 WRITE (*,*) 'Compute area of each generation'
 
 do14: DO i = 0,  numgen
 
 genarea(i) = pi * 0.25D0 * gendia(i)**2
 
 sumarea(i) = genarea(i) * (2**i)
 
 if141: IF (i == 0) THEN 
 
  totala(i) = sumarea(i)
  
 ELSE
 
   totala(i) = totala(i-1) + sumarea(i)
   
 END IF if141
 
 END DO do14
 
 WRITE (*,*) 'computing alveolar volume per unit length'
 ! for other terms
 do021: DO i = 1, numgen
 
 avpul(i) = dtotalvdz(i) * (totalv(numgen) + TV) /genlen(i)
 
 END DO do021

! Writing this data to file
 WRITE (*,*) 'Write area of each generation'
 
! OPEN (UNIT = 555, FILE = '666-genarea.dat', STATUS = 'REPLACE', FORM = 'FORMATTED')
 
! WRITE (555, *) '#gen, gen area, sum area of that generation, alveolar volume per unit length'

! do15: DO i = 0,  numgen

!   WRITE (555,*) i, genarea(i), sumarea(i), avpul(i)

! END DO do15

! CLOSE(555)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! PART 3 : Computing deposition probabilites

 WRITE (*,*) 'Compute alpha for each generation'

! normalize alpha
 dsum = 0.D0
 
 do040: DO i = 0, numgen
 
   alpha(i) = 0.241D0 * (gendia(i)/genlen(i)) * (totalv(numgen)/(totala(numgen) * daa)) * dtotalvdz(i)
 
   dsum = dsum + alpha(i)
 
 END DO do040
 
 WRITE (*,*) 'Compute flow rate for each generation'
!compute generation velocity and stokes number
 do05: DO i = 0, numgen
 
   genQa(i) = (1.D0 - totalvfrac(i)) * Qa
   
   genu(i) = genQa(i)/(sumarea(i) + avpul(i))
   
   genstk(i) =  taup * genu(i)/ gendia(i)

 END DO do05
 
 
! compute deposition efficiencies

 WRITE (*,*) 'Compute deposition efficiencies'
 
! OPEN (UNIT = 555, FILE = '666-depeffdebug.dat', STATUS = 'REPLACE', FORM = 'FORMATTED')
 
! WRITE (555,*) '#gen, numerator, denominator, ratio, exponent, nsde'
 
 do06: DO i = 0, numgen
  
   if061: IF (genstk(i) > 0.0001) THEN 
   
   nide(i) = 1.3D0 * (genstk(i) - 0.0001D0)/(1.D0 - alpha(i))
   
   ELSE 
   
   nide(i) = 0.D0
   
   END IF if061
   
   if062: IF (genQa(i) .NE. 0) THEN 
   
     ndde(i) = 1.D0 - DEXP( -36.D0 * Dbrown * genlen(i) * sumarea(i) / (genarea(i) * genQa(i)))
     
     a1 = -2.D0 * g *  taup * sumarea(i) * genlen(i)
     
     a2 = (DSQRT( pi * genarea(i)) * genQa(i))
     
     a3 = a1/a2
     
     nsde(i) = 1.D0 - DEXP(a3)
     
!     WRITE (555,*) i, a1, a2, a3, DEXP(a3), nsde(i)
   
   ELSE
   
     ndde(i) = 1.D0 
     
     nsde(i) = 1.D0
     
     a1 = -2.D0 * g *  taup * sumarea(i) * genlen(i)
     
     a2 = 0.D0
     
     a3 = 0.D0
     
     nsde(i) = 1.D0
     
!     WRITE (555,*) i, a1, a2, a3, DEXP(a3), nsde(i)

   END IF if062
   
   nde(i) = nide(i) + ndde(i) + nsde(i)
   
 END DO do06
 
! CLOSE(555)
 
! WRITE (*,*) 'Write alpha, velocity and stokes number, deposition efficiencies and deposition probability for each generation to file'

! OPEN (UNIT = 555, FILE = '666-genalpha.dat', STATUS = 'REPLACE', FORM = 'FORMATTED')
 
! WRITE (555, *) '#gen, gen alpha, gen air velocity, gen Qa, gen stokes number, nide, ndde, nsde, nde, Pdep'
 
 do04: DO i = 0, numgen
 
 alpha(i) = alpha(i)/dsum
 
 Pdep(i) = nde(i) * (1.D0 - alpha(i))
 
! WRITE (555, *) i, alpha(i), genu(i), genQa(i), genstk(i), nide(i), ndde(i), nsde(i), nde(i), Pdep(i)
 
 END DO do04
 
! CLOSE(555)

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 ifimpos: IF (2<3) THEN 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! PART 4 : Adding variability to each area

 WRITE (*,*) 'Computing airway properties with variability'
! Generate random numbers and use them
 
! Intialize random number generator
  CALL r4_nor_setup ( kn, fn, wn )
! set seed 
  seed = 1234567629

  prow = 1
  
  do16: DO i = 0, numgen
  
    do161: DO j = 1, 2**i
    
      rand = r4_nor ( seed, kn, fn, wn )
      binarea(prow, 1) = i
      binarea(prow, 2) = j
      binarea(prow,3)  = genarea(i) * (1.D0 + sig * rand)
      
      bincount(prow, 1) = i
      bincount(prow, 2) = j
      bincount(prow, 3) = 0
     
      prow = prow + 1
  
    END DO do161
    
  END DO do16

 WRITE (*,*) 'Writing airway areas to file'
! Writing this data to file
! OPEN (UNIT = 555, FILE = '666-genarea-variability.dat', STATUS = 'REPLACE', FORM = 'FORMATTED')

! do17: DO i = 1, nbins
 
!  WRITE (555,*) "#generation, gen number, bin number, bin area"

!  WRITE (555,*) i, binarea(i, 1), binarea(i,2), binarea(i,3)

! END DO do17
 
! CLOSE(555)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! Computing area ratios, to be used in calculations
! Saves computational effort

! Computing area ratios

 WRITE (*,*) 'Computing area ratios'
 
! For zeroth bin
  i = 1
  crow1 = 2
  crow2 = 3
  
  binarearatios(i,1) =  0
  binarearatios(i,2) =  1
  binarearatios(i,3) =  binarea(crow1,3)/(binarea(crow1,3) +  binarea(crow2,3))

!For other bins in the interior
  do18: DO i = 1, (numgen-1)
    
   do181: DO j = 1, 2**i
   
      prow = 2**i - 1 + j 
      crow1 = 2**(i+1) - 1 + 2*j - 1
      crow2 = crow1 + 1
      
      arratio = binarea(crow1,3)/(binarea(crow1,3) + binarea(crow2,3))
    
      binarearatios(prow, 1) = i
      binarearatios(prow, 2) = j
      binarearatios(prow, 3) = arratio
      
    END DO do181
    
  END DO do18
 
! Writing this data to file
! OPEN (UNIT = 555, FILE = '666-genarea-ratios.dat', STATUS = 'REPLACE', FORM = 'FORMATTED')
 
! WRITE (555,*) "#generation, gen number, bin number, children area ration"

! do19: DO i = 1, nareabins

!   WRITE (555,*) i, binarearatios(i, 1), binarearatios(i,2), binarearatios(i,3)

! END DO do19
 
! CLOSE(555)

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 END IF ifimpos
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! PART 6 - Writing all data to file
! WRITE (*,*) 'Write all data to file'
 
! OPEN (UNIT = 555, FILE = '666-alldata.dat', STATUS = 'REPLACE', FORM = 'FORMATTED')
 
! WRITE (555, *) '# gen, length, diameter, area at each gen, total area of that gen, total area till than gen, alveolar volume at each gen, alveolar volume till than gen, fraction of alveolar volume till that generation, alveolar volume per unit length of airway, alpha, air flow rate at that gen, velocity, dtotalvdz, stokes number, nide, nsde, ndde, nde, P'
 
! do30:  DO i = 0, numgen
 
! WRITE (555,*) i, genlen(i), gendia(i), genarea(i), sumarea(i), totala(i), genalv(i), totalv(i), totalvfrac(i), avpul(i), alpha(i), genQa(i), genu(i), dtotalvdz(i), genstk(i), nide(i), nsde(i), ndde(i), nde(i), Pdep(i)
 
! END DO do30
 
! CLOSE(555)

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! PART 7 - Write simulation parameters to file

 WRITE (*,*) 'Write simulation parameters to file'
 
 OPEN (UNIT = 555, FILE = '666-simulation-parameters.dat', STATUS = 'REPLACE', FORM = 'FORMATTED')
 
 WRITE (555, *) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
 
 WRITE (555,*) 'Standard Deviation sigma is' , sig
 
 WRITE (555,*) 'No of random numbers (particle) is' , nrandoms
  
 WRITE (555,*) 'No of bins is' , nbins

 WRITE (555,*) 'Particle relaxation time is' , taup
 
 WRITE (555,*) 'Extra thoracic deposition efficiency' , etaeth
 
 WRITE (555,*) 'Alveolar deposition efficiency' , etaalv
 
 WRITE (555,*) 'Exp of 1 is', DEXP(1.D0)

 WRITE (555, *) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'

 CLOSE(555) 

 CALL CPU_TIME(end_time)

 WRITE (*,*) 'Time for initialization is', end_time - start_time
 
END SUBROUTINE sub_lm_initialize
