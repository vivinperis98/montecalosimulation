`PROGRAM lungmodel_deposition
! Monte carlo model for particle deposition in lung
!
 USE lungmodel_properties
 
 IMPLICIT NONE
 
 DOUBLE PRECISION :: rand, rand2, rand3
 
 DOUBLE PRECISION :: start_time, end_time, run_time
  
 CALL CPU_TIME(start_time)
 
! INTIALIZING PARAMETERS
 WRITE (*,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~' 
 WRITE (*,*) 'Initializing parameters'
 
 CALL sub_lm_initialize
 
 WRITE (*,*) 'Finished initializing parameters'
 WRITE (*,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~' 
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 ifimpos: IF (2 < 3) THEN
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
! Generate random numbers and sort
 WRITE (*,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
 WRITE (*,*) 'Starting Monte-Carlo simulation'

! Initialize zeroth row
 bincount(1,1) = 0
 bincount(1,2) = 1
 bincount(1,3) = nrandoms 

!&&&&&&&&&&&&&&&&&&&&&&&&  Part 1   &&&&&&&&&&&&&&&&
! For generations 0 to 16, that do not have alveoli
 do201: DO i = 0, 16
 
   do2011: DO j = 1, 2**i

! find indices of parent row and children row
      prow = 2**i - 1 + j 
      crow1 = 2**(i+1) - 1 + 2*j - 1
      crow2 = crow1 + 1

! initialize no of particles to no of particles in parent bin
      ncount = bincount(prow, 3)
! no of particles deposited in that bin set to zero
      depcount = 0
  
! start deposition / transmission simulation
      do20111: DO k = 1, ncount
! if condition: particle deposited in that bin itself, if random number less than Pdep
        CALL RANDOM_NUMBER(rand2)
       
        if201110: IF(1>2) THEN
       
          depcount = depcount + 1

! else: particle moves to one of the two children bins, based on another random number
	ELSE
    
          CALL RANDOM_NUMBER(rand)
        
          if201111: IF (rand < binarearatios(prow,3)) THEN
        
            bincount(crow1,3) = bincount(crow1,3) + 1
          
          ELSE
        
            bincount(crow2,3) = bincount(crow2,3) + 1
          
          END IF if201111
          
        END IF if201110
        
      END DO do20111

! end if: set count of particles in that bin to number of particles deposited in that bin
      bincount(prow, 3) = depcount
   
   END DO do2011
 
 END DO do201
!&&&&&&&&&&&&&&&&&&&&&&&&  Part 2   &&&&&&&&&&&&&&&&
!For generations 17 to 23 that are alveolated
  do202: DO i = 17, numgen-1
 
   do2021: DO j = 1, 2**i

! find indices of parent row and children row
      prow = 2**i - 1 + j 
      crow1 = 2**(i+1) - 1 + 2*j - 1
      crow2 = crow1 + 1

! initialize no of particles to no of particles in parent bin
      ncount = bincount(prow, 3)
! no of particles deposited in that bin set to zero
      depcount = 0
      
! start deposition / transmission simulation
      do20211: DO k = 1, ncount
! if condition: particle deposited in that bin itself, if random number less than Pdep
        CALL RANDOM_NUMBER(rand2)
       
        if202110: IF (1>2) THEN
       
          depcount = depcount + 1

! else: particle gets deposited in the alveoli or moves down
	ELSE
	
	  CALL RANDOM_NUMBER(rand3)
! if condition: particle gets deposited in the alveoli	  
	  if2021101: IF (1>2) THEN
	  
! set generation at which this particle will be deposited
	    if20211010: IF (i == 17) THEN
	    
	      gen17count(j) = gen17count(j) + 1
	      
	    ELSE IF (i==18) THEN 
	    
    	      gen18count(j) = gen18count(j) + 1
    	      
    	    ELSE IF (i==19) THEN
    	    
    	      gen19count(j) = gen19count(j) + 1
    	      
    	    ELSE IF (i==20) THEN
    	    
    	      gen20count(j) = gen20count(j) + 1
    	      
    	    ELSE IF (i==21) THEN
    	    
    	      gen21count(j) = gen21count(j) + 1
    	      
    	   ELSE 
    	   
    	      gen22count(j) = gen22count(j) + 1
    	      
    	   END IF if20211010

! else: particle moves down to another generation	  
	  ELSE
    
          CALL RANDOM_NUMBER(rand)
        
          if202111: IF (rand < binarearatios(prow,3)) THEN
        
            bincount(crow1,3) = bincount(crow1,3) + 1
          
          ELSE
        
            bincount(crow2,3) = bincount(crow2,3) + 1
          
          END IF if202111
          
          END IF if2021101
          
        END IF if202110
        
      END DO do20211

! end if: set count of particles in that bin to number of particles deposited in that bin
      bincount(prow, 3) = depcount
      
      ifcheckmax: IF(airwaymax(i) < depcount) THEN
      
        airwaymax(i) = depcount
      
      END IF ifcheckmax

   END DO do2021
   
  END DO do202


! For the last generation
 i = numgen
 
 do203: DO j = 1, 2**i
 
   prow = 2**i - 1 + j 
 
   gen23count(j) = bincount(prow,3) 
   
   ifcheckmax3: IF(airwaymax(i) < gen23count(j)) THEN
      
        airwaymax(i) = gen23count(j)
      
  END IF ifcheckmax3
 
 END DO do203

 WRITE (*,*) 'Finished Monte-Carlo simulation'
 WRITE (*,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
 
 CALL CPU_TIME(end_time)

 WRITE (*,*) 'Time for running program is', end_time - start_time
 
 run_time = end_time - start_time
 
 CALL CPU_TIME(start_time)

! Writing results
 WRITE (*,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
 WRITE (*,*) 'Starting to write results to data file'



!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!~~~~~~~~~~~~~~~~~~~~
 OPEN (UNIT = 555, FILE = '666-gen23alv-counts.dat', STATUS = 'REPLACE', FORM = 'FORMATTED')
 
 WRITE (555, *) '#j  gen23alveolicount'

 i = 23
  
  do92: DO j = 1, 2**i
 	prow = 2**i - 1 + j 
     WRITE (555, *) j,'\n', bincount(prow,3)
     
 END DO do92
 
 CLOSE(555)
!~~~~~~~~~~~~~~~~~~~~

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 WRITE (*,*) 'Finished writing  results to data file'
 WRITE (*,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'

 CALL CPU_TIME(end_time)
 
 WRITE (*,*) 'Write simulation parameters to file'
 
 OPEN (UNIT = 555, FILE = '666-simulation-parameters.dat', STATUS = 'OLD', FORM = 'FORMATTED', POSITION = 'APPEND')
 
 WRITE (555,*) 'Time taken for Monte Carlo simulation is ' , run_time
 
 WRITE (555, *) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~' 
 WRITE (555, *) 'Maximum number of particles in alveoli at each gen is'
 
 WRITE (555, *) '17th gen', MAXVAL(gen17count)
 
 WRITE (555, *) '18th gen', MAXVAL(gen18count)
 
 WRITE (555, *) '19th gen', MAXVAL(gen19count)
 
 WRITE (555, *) '20th gen', MAXVAL(gen20count)
 
 WRITE (555, *) '21st gen', MAXVAL(gen21count)
 
 WRITE (555, *) '22st gen', MAXVAL(gen22count)
 
 WRITE (555, *) '23rd gen', MAXVAL(gen23count)

 WRITE (555, *) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~' 
 WRITE (555, *) 'Maximum number of particles in airways at each gen is' 
 
 dow: DO i=17,23
  
   WRITE (555, *) 'gen, value', i, airwaymax(i)
 
 END DO dow
 
 WRITE (555, *) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'

 CLOSE(555) 

 WRITE (*,*) 'Time for writing data is', end_time - start_time

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 END IF ifimpos
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
END PROGRAM
