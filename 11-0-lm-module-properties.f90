MODULE lungmodel_properties
! Monte carlo model for particle deposition in lung
 
 IMPLICIT NONE

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

! Parameter constants
 DOUBLE PRECISION, PARAMETER :: pi = DACOS(-1.D0)		! value of pi
 DOUBLE PRECISION, PARAMETER :: g = 980.D0			! gravity (cm/s2)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 DOUBLE PRECISION, PARAMETER :: p3  = 0.388D0
 DOUBLE PRECISION, PARAMETER :: p4  = 0.2929D0
 DOUBLE PRECISION, PARAMETER :: p5  = 0.00634D0
 DOUBLE PRECISION, PARAMETER :: d00 = 1.8D0
 DOUBLE PRECISION, PARAMETER :: d01 = 1.3D0

! Lung - length variables
 DOUBLE PRECISION, PARAMETER :: p1  = -0.92D0
 DOUBLE PRECISION, PARAMETER :: p2  = -0.17D0
 DOUBLE PRECISION, PARAMETER :: l0  = 12.D0
 DOUBLE PRECISION, PARAMETER :: l0p = 2.5D0

! Lung - Alveoli variables
 DOUBLE PRECISION, PARAMETER :: daa = 0.0272D0					! diameter of alveoli
 DOUBLE PRECISION, PARAMETER :: Vaa = (4.D0/3.D0) * pi * ((daa/2.D0)**3)	! volume of alveoli
 DOUBLE PRECISION, PARAMETER :: TV = 500.D0					! tidal volume

! Flow - air properties
 DOUBLE PRECISION, PARAMETER :: rhoa = 0.91D0 		! density of air (g/cc)
 DOUBLE PRECISION, PARAMETER :: mua = 1.8D0 * 1e-4 	! dynamic viscocity of air (g/cc/s)
 DOUBLE PRECISION, PARAMETER :: Qa = 250.D0 		! air flow rate (cc/s)
 DOUBLE PRECISION, PARAMETER :: Ta = 295.D0		! temperature of air

! Flow - particle properties
 DOUBLE PRECISION, PARAMETER :: dp = 0.5D0 * 1e-4  				! diameter of particles (cm)
 DOUBLE PRECISION, PARAMETER :: taup = rhoa * (dp**2) / (18.D0 * mua)  		! relaxation time for particles
 DOUBLE PRECISION, PARAMETER :: csf = 1.D0 + ((2.52D0 * 0.0000067D0)/dp)  	! cunningham slip factor
 DOUBLE PRECISION, PARAMETER :: kboltz = 1.38D0 * 1e-16			  	! boltzmann constant
 DOUBLE PRECISION, PARAMETER :: Dbrown = (kboltz * Ta * csf)/(3.D0 * pi * mua * dp) ! brownian diffusion coefficient
 
 
! Extra thoracic deposition
! This is an empirical equation. some variables converted to SI Units : dp in microns, Qa in lit/min
 DOUBLE PRECISION, PARAMETER :: etaeth = DEXP(-0.000278D0 * rhoa * ((dp/1e4)**2) * (Qa*0.06D0)  - 20.4D0 * (Dbrown**0.66D0) * ((Qa*0.06D0)**(-0.31D0)))

! Alveolar deposition
! This value must be checked and updated later.
 DOUBLE PRECISION, PARAMETER :: etaalv = 0.228D0
 
! Simulation parameters
 INTEGER, PARAMETER :: numgen=23			! no of generations
 INTEGER, PARAMETER :: nbins = 2**(numgen+1) - 1	! total no of bins (airways)
! INTEGER, PARAMETER :: nasins = 2**(numgen) 		! total no of asinus bins (last gen collection near blood barrier)
 INTEGER, PARAMETER :: nareabins = 2**(numgen) - 1    	! total no of area bins


 DOUBLE PRECISION, DIMENSION(0:numgen) :: nal		! no of alveolar ducts
 DOUBLE PRECISION, DIMENSION(0:numgen) :: genalv	! alveolar volume in a generation (sum of all alveoli in that gen)
 DOUBLE PRECISION, DIMENSION(0:numgen) :: totalv	! total alveolar volume till that generation
 DOUBLE PRECISION, DIMENSION(0:numgen) :: totalvfrac	! fraction of total alveolar volume till that generation
 DOUBLE PRECISION, DIMENSION(0:numgen) :: totala	! area of ducts upto that generation
 DOUBLE PRECISION, DIMENSION(0:numgen) :: dtotalvdz	! rate of change of volume
 DOUBLE PRECISION, DIMENSION(0:numgen) :: alpha		! fraction of airway surface that is alveolated
 DOUBLE PRECISION, DIMENSION(0:numgen) :: avpul		! alveolar volume per unit length of airway
 DOUBLE PRECISION, DIMENSION(0:numgen) :: gendep	! deposition at each generation

 DOUBLE PRECISION, DIMENSION(0:numgen) :: gendia	! diameter of each generation
 DOUBLE PRECISION, DIMENSION(0:numgen) :: genlen	! length of each generation
 DOUBLE PRECISION, DIMENSION(0:numgen) :: genarea	! area of each generation
 DOUBLE PRECISION, DIMENSION(0:numgen) :: sumarea	! total area of airways in each generation (sum of areas of all airways in that generation)

 DOUBLE PRECISION, DIMENSION(0:numgen) :: genQa		! air flow rate (cc/s) at each generation
 DOUBLE PRECISION, DIMENSION(0:numgen) :: genu		! air velocity at each generation
 DOUBLE PRECISION, DIMENSION(0:numgen) :: genstk	! stokes number at each generation
 
 DOUBLE PRECISION, DIMENSION(0:numgen) :: nide		! impaction deposition efficieny
 DOUBLE PRECISION, DIMENSION(0:numgen) :: ndde		! diffusion deposition efficieny
 DOUBLE PRECISION, DIMENSION(0:numgen) :: nsde		! sedimentation deposition efficieny
 DOUBLE PRECISION, DIMENSION(0:numgen) :: nde		! total deposition efficieny - sum of all the above three
 DOUBLE PRECISION, DIMENSION(0:numgen) :: Pdep		! Probability of deposition in that generation
 
 DOUBLE PRECISION, DIMENSION(nbins,3) :: binarea	! array to store bin area data
 DOUBLE PRECISION, DIMENSION(nbins,3) :: bincount	! array to store count of particles in each bin (airways)
! DOUBLE PRECISION, DIMENSION(nasins) :: asincount	! array to store no of particles in asinus bins
 DOUBLE PRECISION, DIMENSION(nareabins,3) :: binarearatios	! array to store ratio of bin areas in each generation
 
! Arrays to store alveolar deposition
 DOUBLE PRECISION, DIMENSION(2**17) :: gen17count
 DOUBLE PRECISION, DIMENSION(2**18) :: gen18count
 DOUBLE PRECISION, DIMENSION(2**19) :: gen19count
 DOUBLE PRECISION, DIMENSION(2**20) :: gen20count
 DOUBLE PRECISION, DIMENSION(2**21) :: gen21count
 DOUBLE PRECISION, DIMENSION(2**22) :: gen22count
 DOUBLE PRECISION, DIMENSION(2**23) :: gen23count
 
 DOUBLE PRECISION, DIMENSION(17:23) :: airwaymax
 
 INTEGER(KIND=8) :: i, j, k, prow, crow1, crow2, ncount, depcount
 DOUBLE PRECISION :: arratio, endt, start, dsum, checknrandoms
 
 
END MODULE lungmodel_properties
